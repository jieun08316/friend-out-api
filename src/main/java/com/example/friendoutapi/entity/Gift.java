package com.example.friendoutapi.entity;


import com.example.friendoutapi.eums.ExchangeStatus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CurrentTimestamp;

import java.time.LocalDate;

@Entity
@Getter
@Setter

public class Gift {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "friendId")
    private Friend friend;

    @Enumerated
    @Column(nullable = false)
    private ExchangeStatus exchange;

    @Column(nullable = false)
    private String content;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private LocalDate giftDate;

}

