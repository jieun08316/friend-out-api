package com.example.friendoutapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FriendCreatRequest {
    private String name;
    private String birthDay;
    private String phoneNumber;
    private String etcMemo;
    private Boolean isCutFriend;
    private String cutReason;


}
