package com.example.friendoutapi.model;


import com.example.friendoutapi.entity.Friend;
import com.example.friendoutapi.eums.ExchangeStatus;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class GiftCreatRequest {

    private Friend friend;

    private ExchangeStatus exchange;

    private String content;

    private Double price;

    private LocalDate giftDate;

}
