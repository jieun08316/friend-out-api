package com.example.friendoutapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FriendOutApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FriendOutApiApplication.class, args);
	}

}
