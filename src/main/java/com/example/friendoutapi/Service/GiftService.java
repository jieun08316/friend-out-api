package com.example.friendoutapi.Service;

import com.example.friendoutapi.entity.Friend;
import com.example.friendoutapi.entity.Gift;
import com.example.friendoutapi.model.GiftCreatRequest;
import com.example.friendoutapi.repository.GiftRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GiftService {

    private final GiftRepository giftRepository;

    public void setGift(Friend friend, GiftCreatRequest request) {
        Gift addData = new Gift();
        addData.setExchange(request.getExchange());
        addData.setContent(request.getContent());
        addData.setPrice(request.getPrice());
        addData.setGiftDate(request.getGiftDate());

        giftRepository.save(addData);
    }

}
