package com.example.friendoutapi.Service;

import com.example.friendoutapi.entity.Friend;
import com.example.friendoutapi.model.FriendCreatRequest;
import com.example.friendoutapi.repository.FriendRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FriendService {
    private final FriendRepository friendRepository;

    public void setFriend(FriendCreatRequest request) {
        Friend addData = new Friend();
        addData.setName(request.getName());
        addData.setBirthDay(request.getBirthDay());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setEtcMemo(request.getEtcMemo());
        addData.setIsCutFriend(request.getIsCutFriend());
        addData.setCutReason(request.getCutReason());

        friendRepository.save(addData);
    }

}
