package com.example.friendoutapi.eums;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.swing.*;

@Getter
@AllArgsConstructor
public enum ExchangeStatus {
    GIVE("주다"),
    RECEIVE("받다");

        private String name;

}
