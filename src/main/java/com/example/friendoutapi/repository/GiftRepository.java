package com.example.friendoutapi.repository;

import com.example.friendoutapi.entity.Gift;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GiftRepository extends JpaRepository<Gift, Long> {
}
