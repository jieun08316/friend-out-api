package com.example.friendoutapi.controller;

import com.example.friendoutapi.Service.GiftService;
import com.example.friendoutapi.model.GiftCreatRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/Gift")
public class GiftController {
    private final GiftService giftService;

    @PostMapping("/info")
    public String setGift(@RequestBody GiftCreatRequest request){
        giftService.setGift(request);
        return "OK";
    }
}
