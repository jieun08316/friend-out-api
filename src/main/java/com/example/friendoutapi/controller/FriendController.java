package com.example.friendoutapi.controller;

import com.example.friendoutapi.Service.FriendService;
import com.example.friendoutapi.model.FriendCreatRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/friend")
public class FriendController {
    private final FriendService friendService;

    @PostMapping("/info")
    public String setFriend(@RequestBody FriendCreatRequest request){
        friendService.setFriend(request);
        return "OK";
    }
}
